# Reinstalling Windows 10 after Linux

## Prepare and reinstall Windows

* Windows 10 reinstall overview: https://support.microsoft.com/en-in/help/4000735/windows-10-reinstall
* Create installtion media for Windows: https://support.microsoft.com/en-in/help/15088/windows-10-create-installation-media

## Grub

* Repair Grub: https://askubuntu.com/questions/88384/how-can-i-repair-grub-how-to-get-ubuntu-back-after-installing-windows
